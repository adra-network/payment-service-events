# Payment Service Events

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]

The event library for events emitted by payment service
## Installation

Via Composer

``` bash
$ composer require adranetwork/payment-service-events
```

## Credits

- [Francois Auclair][link-author]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/adranetwork/payment-service-events.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/adranetwork/payment-service-events.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/adranetwork/payment-service-events/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/adranetwork/payment-service-events
[link-downloads]: https://packagist.org/packages/adranetwork/payment-service-events
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://fauclair.com
