<?php

namespace Adranetwork\PaymentServiceEvents;

use Adranetwork\AdraCloud\EventSource\Event;
use Adranetwork\AdraCloud\EventSource\StreamEvent;
use Adranetwork\PaymentServiceEvents\DTO\DonationIntentDTO;


abstract class DonationIntentEvent extends StreamEvent implements Event
{
    public DonationIntentDTO $donationIntentDTO;

    public function __construct(
        array $attributes = []
    )
    {
        parent::__construct();
        $this->donationIntentDTO = DonationIntentDTO::from($attributes);

    }

    public function jsonSerialize(): array
    {
        return $this->donationIntentDTO->jsonSerialize();
    }

}
