<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Adranetwork\PaymentServiceEvents\Enums\DonationType;
use Spatie\LaravelData\Data;


/**
 * Pric
 * @property string $referenceId Unique identifier for the object.
 */

class PaymentProviderDTO extends Data
{

    public function __construct(
        public PaymentProvider $name,
        public ?string $referenceId = null,
        public ?string $accountId = null,
        public ?string $checkoutSessionId = null,
        public ?string $invoiceId = null,
        public ?string $customerId = null,
    )
    {
    }

}

