<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\AdraCloud\Enums\PaymentStatus;
use Illuminate\Http\Request;
use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapInputName(SnakeCaseMapper::class)]
class DonationIntentDTO extends Data
{

    public function __construct(
        public PaymentProviderDTO $paymentProvider,
        public InternalReferenceDTO $internalReferences,
        public DonationDTO $donation,
        public PaymentStatus $status = PaymentStatus::UNKNOWN,
        public ?FingerprintDTO $fingerprint = null,
        public ?PhoneDTO $phone = null,
        public ?AddressDTO $address = null,
        public ?DonorDTO $donor = null,
        public ?ConsentDTO $consents = null,
        public ?UtmDTO $utms = null,
        public ?string $id = null,
        public ?string $createdAt = null,

    )
    {
    }

    public static function fromStripeCheckoutConfirmedRequest(Request $request): self
    {
        return self::from([
            ...$request->all(),
            'paymentProvider' => [
                ...$request->input('payment_provider', $request->input('paymentProvider')),
                'name' => PaymentProvider::STRIPE,
                'object' => 'checkout.session',
            ],
            'status' => PaymentStatus::SUCCEEDED
        ]);
    }

    public static function fromStripeCheckoutCreateRequest(Request $request): self
    {
        return self::from([
            ...$request->all(),
            'status' => PaymentStatus::PROCESSING //
        ]);
    }

}
