<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Data;

class ConsentDTO extends Data
{
    public function __construct(
        public readonly ?bool $communication = false,
        public readonly ?bool $gdpr = false,

    )
    {}
}

