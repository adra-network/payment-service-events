<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Spatie\LaravelData\Data;

class DonorFormDTO extends Data
{

    public function __construct(
        public string $email,
        public string $first_name,
        public string $last_name,
        public ?bool $gdpr,
    )
    {}

}
