<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Exception;
use Spatie\LaravelData\Data;

class PhoneDTO extends Data
{

    public function __construct(
        public readonly ?string $number = null,
        public readonly ?string $countryCallingCode = null,
        public readonly ?string $extension = null,
    )
    {}

    /**
     * This will return an E164 Standard string excluding the extension (as not part of standard)
     * https://en.wikipedia.org/wiki/E.164
     * @return string
     * @throws Exception
     */
    public function toE164(): string
    {
        if (!$this->number) {
            throw new Exception('Could not format phone to E164. Missing required value for number');
        }
        if (!$this->countryCallingCode) {
            throw new Exception('Could not format phone to E164. Missing required value for countyCallingCode');
        }
        return sprintf('+%s%s',
                 $this->countryCallingCode,
                $this->number,
            );
    }
}

