<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Data;

class UtmDTO extends Data
{
    public function __construct(
        public readonly ?string $medium = null,
        public readonly ?string $source = null,
        public readonly ?string $content = null,
        public readonly ?string $campaign = null,
        public readonly ?array $terms = [],

    )
    {}
}
