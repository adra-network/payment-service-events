<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Data;

class ConfirmDTO extends Data
{
    public function __construct(
        public string $gateway,
        public string $donation_intent_id,
        public string $organization_id,
    )
    {}
}
