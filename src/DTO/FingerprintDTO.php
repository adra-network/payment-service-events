<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Data;

class FingerprintDTO extends Data
{
    public function __construct(
        public readonly ?string $visitor_id = null,
        public readonly ?string $certainty = null,
    )
    {}
}

