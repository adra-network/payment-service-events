<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Data;

class ProcessDTO extends Data
{
    public function __construct(
        public string $gateway,
        public array $donation,
        public array $donor,
        public array $payment_provider,
        public array $page,
        public array $utms,
        public ?string $donation_intent_id =null,
        public ?array $fingerprint = null,

    )
    {}
}
