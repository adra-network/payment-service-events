<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;

class SystemConfigurationDTO extends Data
{
    public function __construct(
        public string $id,
        public string $key,
        #[MapName('ssm_path')] // map path to ssm_path for backward compatibility
        public string $path,
        public bool $encrypt,
    )
    {}
}
