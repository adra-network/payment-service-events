<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Spatie\LaravelData\Data;

class DonorInfoDTO extends Data
{
    public function __construct(
        public ?string $first_name = null,
        public ?string $last_name = null,
        public ?string $email  = null,
        public ?string $title  = null,
        public ?string $birthday = null,
        public ?bool $communication_consent = false,
        public ?bool $gdpr_consent  = false,

    )
    {}
}

