<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Spatie\LaravelData\Data;

class InternalReferenceDTO extends Data
{
    public function __construct(
        public readonly ?string $donationPageId = null,
        public readonly ?string $organizationId  = null,
    )
    {}
}

