<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Attributes\MapOutputName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapInputName(SnakeCaseMapper::class)]
#[MapOutputName(SnakeCaseMapper::class)]

class DonorDTO extends Data
{
    public function __construct(
        public ?string $email = null,
        public ?string $firstName = null,
        public ?string $lastName = null,
        public ?string $title = null,
        public ?string $birthday = null,
        public ?string $languageCode = null,
    )
    {
    }

    public function fullName(): string
    {
        return trim(sprintf('%s %s',
            $this->firstName,
            $this->lastName
        ));
    }

    public function maskedEmail(): ?string
    {
        if (!$this->email) {
            return null;
        }
        [$local, $domain] = explode('@', $this->email);
        $length = strlen($local);
        $obfuscated = substr($local, 0, floor($length / 2));
        $obfuscated .= str_repeat('*', ceil($length / 2));
        return $obfuscated . '@' . $domain;
    }


}
