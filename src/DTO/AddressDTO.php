<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use PrinsFrank\Standards\Country\ISO3166_1_Alpha_3;
use Spatie\LaravelData\Data;

class AddressDTO extends Data
{
    public function __construct(
        public readonly ?string $address_line_1 = null,
        public readonly ?string $address_line_2 = null,
        public readonly ?string $postal_code = null,
        public readonly ?string $dependent_locality = null,
        public readonly ?string $locality = null,
        public readonly ?string $administrative_area = null,
        public readonly ?ISO3166_1_Alpha_3 $country = null,
    )
    {}
}
