<?php

namespace Adranetwork\PaymentServiceEvents\DTO;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\PaymentServiceEvents\DTO\DonorInfoDTO;
use Adranetwork\PaymentServiceEvents\Enums\DonationType;
use Spatie\LaravelData\Data;

class DonationDTO extends Data
{
    public function __construct(
        public int $amount,
        public string $currency,
        public DonationType $donationType,
    )
    {}

    public function formattedAmount()
    {
        return \CurrencyHelper::getHumanReadableAmount($this->currency, $this->amount);
    }
}


