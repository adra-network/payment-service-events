<?php
namespace Adranetwork\PaymentServiceEvents\Objects;



class Donor implements EventObject
{
    public readonly ?string $first_name;
    public readonly ?string $last_name;
    public readonly ?string $email;
    public readonly ?string $title;
    public readonly ?string $birthday;
    public readonly bool $communication_consent;
    public readonly bool $gdpr_consent;

    public function __construct(array $attributes)
    {
        $this->first_name = data_get($attributes, 'first_name');
        $this->last_name = data_get($attributes, 'last_name');
        $this->email = data_get($attributes, 'email');
        $this->title = data_get($attributes, 'title');
        $this->birthday = data_get($attributes, 'birthday');
        $this->communication_consent = data_get($attributes, 'communication') ?? false;
        $this->gdpr_consent = data_get($attributes, 'gdpr') ?? false;
    }


    public function toArray(): array
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'title' => $this->title,
            'birthday' => $this->birthday,
            'communication' => $this->communication_consent,
            'gdpr' => $this->gdpr_consent,
        ];
    }
}
