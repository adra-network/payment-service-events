<?php

namespace Adranetwork\PaymentServiceEvents\Objects;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\AdraCloud\Enums\PaymentStatus;
use PrinsFrank\Standards\Currency\ISO4217_Alpha_3;

class Payment implements EventObject
{
    public int $amount;
    public ISO4217_Alpha_3 $currency;
    public ?PaymentProvider $provider;
    public ?PaymentStatus    $status;
    public readonly ?string $reference_id;


    public function __construct(array $attributes)
    {
        $this->amount = $attributes['amount'];

        $this->currency = ISO4217_Alpha_3::from(
            strtoupper(data_get($attributes, 'currency', ''))
        );

        $this->provider = data_get($attributes, 'provider');

        $this->status = PaymentStatus::tryFrom(data_get($attributes, 'status', ''));

        $this->reference_id = data_get($attributes, 'reference_id');

    }


    public function toArray(): array
    {
        return [
            'amount' => $this->amount,
            'currency' => $this->currency,
            'provider' => $this->provider,
            'status' => $this->status,
            'reference_id' => $this->reference_id,
        ];
    }
}
