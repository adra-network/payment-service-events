<?php

namespace Adranetwork\PaymentServiceEvents\Objects;



use PrinsFrank\Standards\Country\ISO3166_1_Alpha_3;


class Address implements EventObject
{
    public readonly ?string $address_line_1;
    public readonly ?string $address_line_2;
    public readonly ?string $locality;
    public readonly ?string $dependent_locality;
    public readonly ?string $administrative_area;
    public readonly ?ISO3166_1_Alpha_3 $country;
    public readonly ?string $postal_code;

    public function __construct(array $attributes)
    {
        $this->address_line_1 = data_get($attributes, 'address_line_1');
        $this->address_line_2 = data_get($attributes, 'address_line_2');
        $this->locality = data_get($attributes, 'locality');
        $this->dependent_locality = data_get($attributes, 'dependent_locality');
        $this->administrative_area = data_get($attributes, 'administrative_area');
        $this->country = ISO3166_1_Alpha_3::tryFrom(data_get($attributes, 'country', ''));
        $this->postal_code = data_get($attributes, 'postal_code');

    }



    public function toArray(): array
    {
        return [
            'address_line_1' => $this->address_line_1,
            'address_line_2' => $this->address_line_2,
            'dependent_locality' => $this->dependent_locality,
            'administrative_area' => $this->administrative_area,
            'country' => $this->country->value ?? null,
            'postal_code' => $this->postal_code,
        ];
    }
}
