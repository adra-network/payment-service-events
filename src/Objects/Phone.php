<?php

namespace Adranetwork\PaymentServiceEvents\Objects;


class Phone implements EventObject
{
    public readonly ?string $calling_code;
    public readonly ?string $number;
    public readonly ?string $extension;

    public function __construct(array $attributes)
    {
        $this->calling_code = data_get($attributes, 'calling_code');
        $this->number = data_get($attributes, 'number');
        $this->extension = data_get($attributes, 'extension');
    }


    public function toArray(): array
    {
        return (array) $this;
    }
}
