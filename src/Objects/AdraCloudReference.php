<?php

namespace Adranetwork\PaymentServiceEvents\Objects;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\AdraCloud\Enums\PaymentStatus;
use PrinsFrank\Standards\Currency\ISO4217_Alpha_3;

class AdraCloudReference implements EventObject
{
    public readonly string        $organization_id;
    public readonly string        $donation_page_id;
    public readonly ?string        $campaign_id;
    public readonly ?string        $appeal_id;

    public function __construct(array $attributes)
    {
        dd($attributes);
        $this->organization_id = $attributes['organization_id'];
        $this->donation_page_id = $attributes['donation_page_id'];
        $this->campaign_id = data_get($attributes, 'campaign_id');
        $this->appeal_id = data_get($attributes, 'appeal_id');
    }


    public function toArray(): array
    {
        return [
            'organization_id' => $this->organization_id,
            'appeal_id' => $this->appeal_id,
            'campaign_id' => $this->campaign_id,
            'donation_page_id' => $this->donation_page_id,
        ];
    }
}
