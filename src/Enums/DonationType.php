<?php

namespace Adranetwork\PaymentServiceEvents\Enums;

enum DonationType: string
{
    case OneTime = 'one_time';
    case Recurring = 'recurring';
    case Recurring_Setup = 'recurring_setup';


    public function toStripeCheckoutMode(): string
    {
        return match($this) {
            DonationType::OneTime => 'payment',
            DonationType::Recurring => 'subscription'
        };
    }

    public static function fromStripeCheckoutMode(string $mode):self
    {
        return match($mode) {
             'payment' => DonationType::OneTime,
             'subscription' => DonationType::Recurring,
        };
    }
}
