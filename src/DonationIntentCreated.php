<?php

namespace Adranetwork\PaymentServiceEvents;

use Adranetwork\PaymentServiceEvents\Traits\DonationIntentHelper;


class DonationIntentCreated extends DonationIntentEvent
{

   use DonationIntentHelper;

    public function getEventName(): string
    {
        return 'donation-intent.created';
    }

}
