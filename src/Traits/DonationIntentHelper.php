<?php
namespace Adranetwork\PaymentServiceEvents\Traits;

use App\Models\DonationIntent;

trait DonationIntentHelper
{
    public static function fromModel(DonationIntent $model): self
    {
        return new self([
            'id' => $model->id,
            'donor' =>(array) $model->donor,
            'donor_phone' => [
                'calling_code' => $model->donor->phone_calling_code ?? null,
                'number' => $model->donor->phone_number ?? null,
            ],
            'donor_address' => [
                'address_line_1' => $model->donor->address_line_1 ?? null,
                'address_line_2' => $model->donor->address_line_2 ?? null,
                'dependent_locality' => $model->donor->dependent_locality ?? null,
                'administrative_area' => $model->donor->administrative_area ?? null,
                'country' => $model->donor->country ?? null,
                'postal_code' => $model->donor->postal_code ?? null,
            ],
            'reference' => (array)$model->donation,
            'payment' => [
                'amount' => $model->donation->amount ?? null,
                'currency' => $model->donation->currency ?? null,
                'provider' => $model->payment_provider->name ?? null,
                'reference_id' => $model->payment_provider->reference_id ?? null,
                'status' => $model->status->value ?? null,
            ],
            'created_at' => $model->created_at,
        ]);
    }
}
