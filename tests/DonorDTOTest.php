<?php
namespace Adranetwork\PaymentServiceEvents\Tests;

use Adranetwork\PaymentServiceEvents\DTO\DonorDTO;


class DonorDTOTest extends TestCase
{
    /** @test **/
    public function it_can_return_full_name()
    {
        $dto= new DonorDTO(
            firstName: 'john',
            lastName: 'doe'
        );
        $this->assertEquals('john doe', $dto->fullName());
    }
    /** @test **/
    public function full_name_returns_emptyy_string_if_no_first_and_last_name()
    {
        $dto= new DonorDTO(
        );
        $this->assertEquals('', $dto->fullName());
    }

    /** @test **/
    public function full_name_is_trimmed_is_no_last_name_provided()
    {
        $dto= new DonorDTO(
            firstName: 'john',
        );
        $this->assertEquals('john', $dto->fullName());
    }
    /** @test **/
    public function full_name_is_trimmed_is_no_first_name_provided()
    {
        $dto= new DonorDTO(
            lastName: 'doe',
        );
        $this->assertEquals('doe', $dto->fullName());
    }
    /** @test **/
    public function it_mask_half_of_available_email_string_before_the_domain()
    {
        $dto= new DonorDTO(
            email: 'john.doe1@gmail.com' // 9 letters,
        );
        $this->assertEquals('john*****@gmail.com', $dto->maskedEmail());
    }

    /** @test **/
    public function masked_email_nullable_if_no_email()
    {
        $dto= new DonorDTO(
            firstName: 'john.' // 9 letters,
        );
        $this->assertNull($dto->maskedEmail());
    }
}
